var baseUrl = 'http://172.20.10.3:5000/'

$(document).ready(function(){

    httpGet(baseUrl+'led1').then((getResponse) => {
        updateElement(getResponse, baseUrl+'led1');
    }); 

    //httpGet(baseUrl+'gpio/A1').then((getResponse) => {
    //    updateElement(getResponse, baseUrl+'gpio/A1');
    //    let selSwitch = document.getElementById("customSwitchA1");
    //    selSwitch.checked = parseInt(getResponse.value);
    //});

    var ctx = document.getElementById("myChart").getContext('2d');
    window.iot_graphs = new Chart(ctx, config);
    
    $("#on-btn").click(async () => {
        let url = baseUrl+'led1';
        let putResponse = await httpPut(url, {'state': 'on'});
        if (putResponse.ok) {
            let getResponse = await httpGet(url);
            //console.log(getResponse)
            updateElement(getResponse, url);
        }
        else {
            console.error(putResponse)
        }     
    });

    $("#off-btn").click(async () => {
        let url = baseUrl+'led1';
        let putResponse = await httpPut(url, {'state': 'off'});
        if (putResponse.ok) {
            let getResponse = await httpGet(url);
            updateElement(getResponse, url);
        }
        else {
            console.error(putResponse)
        }    
    });

    $('#card-switches').children().on('change', async (data) => {
        let selSwitch = document.getElementById(data.target.id);
        let url = baseUrl + 'gpio/'+selSwitch.id.substr(-2);
        
        if (selSwitch.checked) {
            let putResponse = await httpPut(url, {'value': '1'});
            if (putResponse.ok) {
                let getResponse = await httpGet(url);
                updateElement(getResponse, url);
            } else {
                console.error(putResponse); 
            }
        }
        else {
            let putResponse = await httpPut(url, { 'value': '0' });
            if (putResponse.ok) {
                let getResponse = await httpGet(url);
                updateElement(getResponse, url);
            } else {
                console.error(putResponse); 
            } 
        }
    });

    $("#send-btn").click(() => {
        var input = {"value": document.getElementById('chart-input').value.toString()};
        httpPut(baseUrl+'TEMPERATURE', input);
    })

    $("#update-btn").click(async () => {        
            let getResponse = await httpGet(baseUrl+'TEMPERATURE');
            console.log(getResponse);
            addData(window.iot_graphs, curTime(), getResponse.TEMPERATURE.toString());
            // Possibly Set-interval in JS setInterval(your code, 1000)
            updateLastMessage(`${getResponse.TEMPERATURE.toString()} added to graph`)
        });
    
    setInterval(async () => {        
        let getResponse = await httpGet(baseUrl+'TEMPERATURE');
        console.log(getResponse);
        addData(window.iot_graphs, curTime(), getResponse.TEMPERATURE.toString());
        // Possibly Set-interval in JS setInterval(your code, 1000)
        updateLastMessage(`${getResponse.TEMPERATURE.toString()} added to graph`)
    }, 2000);
});

//HTTP FUNCTIONS
async function httpGet(url){
    try {
        let response = await fetch(url);
        updateLastMessage(`${response.status} ${response.statusText}`);
        let json = await response.json();
        return json;
    } catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

async function httpPut(url, data) {
    try {
        let response = await fetch(url, {
            method: 'PUT', // 'GET', 'PUT', 'DELETE', etc.
            body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
            headers: new Headers({
                'Content-Type': 'application/json;charset=UTF-8'
            })
        });
        updateLastMessage(`${response.status} ${response.statusText}`);
        return response;
    }
    catch (error) {
        console.error(error);
        updateLastMessage(error);
    }
}

//OUTPUTS
function updateElement(data, url){
    // let element = document.getElementById('status-field-'+url.substr(-2)); //element status-field-A0 etc. 
    let element = document.getElementById('status-field-A0'); //element status-field-A0 etc. 
    //value = data.value;
    value = data.LED;
    element.innerHTML = value;
    parseInt(value) ? element.style.backgroundColor = "darkolivegreen" : element.style.backgroundColor = "tomato";
}

function updateLastMessage(text)
{
    let time = curTime();
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time} ${text}`));
    $('#api-log').prepend(li);
}

function addData(chart, label, data) {
    //console.log(chart);
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
        if (dataset.data.length > 10) {
            removeData(chart)
        };  
    });    
    chart.update();
}

function removeData(chart) {
    chart.data.labels.shift();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.shift();
    });
    chart.update();
}

//CHART CONFIG
var config = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'temperature',
            fill: false,
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: [
            ]					
        }
    ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: 'IoT graphs'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Value'
                }
            }]
        }
    }
};

//GET TIME
function curTime (){
    let now = new Date().toLocaleTimeString('da-DK');
    return now;
}